/**
 * ビンゴ
 * @namespace
 */
var Bingo = {};

/**
 * 
 * @enum {number}
 */
Bingo.cellStatus = {
  OPEN            : 1,
  CLOSE           : 0
};


/**
 * コアライブラリ
 * @namespace
 */
Bingo.core = {};


/**
 * ビンゴリーチ判定する。
 *
 * @param {{rank:number, suit:string}[]} card 手札。
 * @returns poker.handCategory のどれか。
 */
Bingo.core.getCardStatus = function(card) {
  // TODO: ここに処理を実装します。
  var cardStatus = {bingo:0, reach:0};

  return cardStatus;
}
