//module("poker.core.getHandCategory");

test("カードが長方形でなければ0,0", function() {
  var card = [
      [1,1,1],
      [1,1,1,1],
      [1,1,1],
      [1,1,1]
    ];
  var cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 0);
  equal(cardStatus.reach, 0);

  card = [
      [1,1,1,1],
      [1,1,1],
      [1,1,1,1]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 0);
  equal(cardStatus.reach, 0);

  card = [
      [1,1,1,1],
      [1,1,1,1],
      [1,1,1,1],
      [1,1,1]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 0);
  equal(cardStatus.reach, 0);
});

test("カードの短辺のサイズが2以下なら0,0", function() {
  var card = [
      [1,1],
      [1,1]
    ];
  var cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 0);
  equal(cardStatus.reach, 0);

  card = [
      [0],
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 0);
  equal(cardStatus.reach, 0);

  card = [
      [],
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 0);
  equal(cardStatus.reach, 0);

  card = [
      [1,1,1,1,1],
      [1,1,1,1,1]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 0);
  equal(cardStatus.reach, 0);

  card = [
      [1,1],
      [1,1],
      [1,1],
      [1,1]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 0);
  equal(cardStatus.reach, 0);
});


test("横ビンゴ1,0", function() {
  var card = [
      [1,1,1],
      [0,0,0],
      [0,0,0]
    ];
  var cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 1);
  equal(cardStatus.reach, 0);

});


test("長方形横ビンゴ1,0", function() {
  var card = [
      [1,1,1,1],
      [0,0,0,0],
      [0,0,0,0]
    ];
  var cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 1);
  equal(cardStatus.reach, 0);

});

test("縦ビンゴ1,0", function() {
  var card = [
      [1,0,0],
      [1,0,0],
      [1,0,0]
    ];
  var cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 1);
  equal(cardStatus.reach, 0);

});

test("長方形縦ビンゴ1,0", function() {
  var card = [
      [1,0,0],
      [1,0,0],
      [1,0,0],
      [1,0,0]
    ];
  var cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 1);
  equal(cardStatus.reach, 0);

});


test("＼ビンゴ1,0", function() {
  var card = [
      [1,0,0],
      [0,1,0],
      [0,0,1]
    ];
  var cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 1);
  equal(cardStatus.reach, 0);

});

test("／ビンゴ1,0", function() {
  var card = [
      [0,0,1],
      [0,1,0],
      [1,0,0]
    ];
  var cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 1);
  equal(cardStatus.reach, 0);

});


test("bingos", function() {
  var card = [
      [0,0,0],
      [0,0,0],
      [1,1,1]
    ];
  var cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 1);
  equal(cardStatus.reach, 0);

  card = [
      [0,0,1],
      [0,0,1],
      [0,0,1]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 1);
  equal(cardStatus.reach, 0);

  card = [
      [1,1,1],
      [1,1,1],
      [1,1,1]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 8);
  equal(cardStatus.reach, 0);

  card = [
      [0,1,0,0],
      [0,1,0,0],
      [0,1,0,0],
      [0,1,0,0]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 1);
  equal(cardStatus.reach, 0);

  card = [
      [1,0,0,0],
      [0,1,0,0],
      [0,0,1,0],
      [0,0,0,1]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 1);
  equal(cardStatus.reach, 0);

  card = [
      [1,1,0,0],
      [0,1,0,0],
      [0,1,0,1],
      [0,1,0,0]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 1);
  equal(cardStatus.reach, 0);

  card = [
      [1,0,0,1],
      [0,1,1,0],
      [0,1,1,0],
      [1,0,0,1]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 2);
  equal(cardStatus.reach, 0);

  card = [
      [0,0,0,0],
      [0,0,0,0],
      [1,1,1,1],
      [1,1,1,1]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 2);
  equal(cardStatus.reach, 0);

  card = [
      [0,0,0,1],
      [0,0,1,0],
      [0,1,0,0],
      [1,1,1,1]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 2);
  equal(cardStatus.reach, 0);

  card = [
      [0,1,1,0],
      [0,1,1,0],
      [0,1,1,0],
      [0,1,1,0]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 2);
  equal(cardStatus.reach, 0);

  card = [
      [1,1,1,1],
      [1,1,1,1],
      [1,1,1,1],
      [1,1,1,1]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 10);
  equal(cardStatus.reach, 0);

  card = [
      [1,0,1,0],
      [0,0,1,0],
      [1,0,1,0],
      [1,1,1,1],
      [0,0,1,0],
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 2);
  equal(cardStatus.reach, 0);

});


test("reaches", function() {

  var card = [
      [1,1,0],
      [0,0,0],
      [0,0,0]
    ];
  var cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 0);
  equal(cardStatus.reach, 1);

  card = [
      [1,0,0],
      [1,0,0],
      [0,0,0]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 0);
  equal(cardStatus.reach, 1);

  card = [
      [1,0,0],
      [0,1,0],
      [0,0,0]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 0);
  equal(cardStatus.reach, 1);

  card = [
      [1,0,0],
      [0,0,0],
      [0,0,1]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 0);
  equal(cardStatus.reach, 1);

  card = [
      [1,0,0],
      [0,0,0],
      [1,0,1]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 0);
  equal(cardStatus.reach, 3);

  card = [
      [1,0,0],
      [1,0,1],
      [0,0,1]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 0);
  equal(cardStatus.reach, 4);

  card = [
      [1,1,0],
      [1,0,1],
      [0,1,1]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 0);
  equal(cardStatus.reach, 7);

  card = [
      [1,0,0,0],
      [1,0,0,0],
      [1,0,0,0],
      [0,0,0,0]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 0);
  equal(cardStatus.reach, 1);

  card = [
      [1,0,0,0],
      [1,1,0,0],
      [1,0,1,0],
      [0,0,0,0]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 0);
  equal(cardStatus.reach, 2);

  card = [
      [1,0,0,1],
      [1,1,1,0],
      [0,0,1,0],
      [1,0,0,0]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 0);
  equal(cardStatus.reach, 4);

  card = [
      [0,1,1,1],
      [1,0,1,1],
      [1,0,0,1],
      [1,1,1,0]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 0);
  equal(cardStatus.reach, 7);

  card = [
      [0,1,1,1,1],
      [1,0,1,1,1],
      [1,1,0,1,1],
      [1,1,1,0,0]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 0);
  equal(cardStatus.reach, 8);

});

test("bingo and reaches", function() {
  var card = [
      [1,1,1],
      [1,0,0],
      [0,0,0]
    ];
  var cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 1);
  equal(cardStatus.reach, 1);

  card = [
      [1,1,1],
      [1,0,0],
      [0,0,1]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 1);
  equal(cardStatus.reach, 3);

  card = [
      [1,1,1],
      [1,1,0],
      [0,0,1]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 2);
  equal(cardStatus.reach, 5);

  card = [
      [1,1,1,1],
      [1,0,0,0],
      [1,0,0,0],
      [0,0,1,0]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 1);
  equal(cardStatus.reach, 1);

  card = [
      [1,1,1,1],
      [1,0,0,1],
      [1,1,0,1],
      [1,1,1,1]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 4);
  equal(cardStatus.reach, 3);

  card = [
      [1,1,1,1],
      [1,0,0,1],
      [1,1,0,1],
      [1,1,1,1],
      [1,1,1,1]
    ];
  cardStatus = Bingo.core.getCardStatus(card);
  equal(cardStatus.bingo, 5);
  equal(cardStatus.reach, 2);

});

